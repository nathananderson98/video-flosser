import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:fraction/fraction.dart';

import 'file_description.dart';

class CoverResultPopup extends StatefulWidget {
  const CoverResultPopup({super.key, required this.cover});

  final File cover;

  @override
  State<CoverResultPopup> createState() => _CoverResultPopupState();
}

class _CoverResultPopupState extends State<CoverResultPopup> {
  late final Uint8List _imagebytes = widget.cover.readAsBytesSync();
  Size? _fileSize;

  @override
  void initState() {
    super.initState();
    _readFileData();
  }

  Future<void> _readFileData() async {
    var decodedImage = await decodeImageFromList(_imagebytes);
    setState(() {
      _fileSize =
          Size(decodedImage.width.toDouble(), decodedImage.height.toDouble());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30),
      child: Center(
        child: Stack(
          children: [
            Image.memory(_imagebytes),
            Positioned(
              bottom: 0,
              child: FileDescription(
                description: {
                  'Cover path': widget.cover.path,
                  'Cover ratio':
                  Fraction.fromDouble(_fileSize?.aspectRatio ?? 0)
                      .reduce()
                      .toString(),
                  'Cover size': _fileSize.toString(),
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}