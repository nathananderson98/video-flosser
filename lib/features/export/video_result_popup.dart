import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fraction/fraction.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:helpers/helpers.dart';
import 'package:video_player/video_player.dart';

import 'file_description.dart';

class VideoResultPopup extends StatefulWidget {
  const VideoResultPopup({super.key, required this.video});

  final File video;

  @override
  State<VideoResultPopup> createState() => _VideoResultPopupState();
}

class _VideoResultPopupState extends State<VideoResultPopup> {
  late VideoPlayerController _controller;
  bool saving = false;
  bool isPlaying = true;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.file(widget.video);
    _controller.initialize().then((_) {
      setState(() {});
      _controller.play();
      _controller.setLooping(true);
    });
  }

  @override
  void dispose() {
    _controller.pause();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30),
      child: Center(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0)
          ),
          child: AspectRatio(
            aspectRatio: _controller.value.aspectRatio,
            child: Stack(
              alignment: Alignment.bottomLeft,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 3.0, right: 3.0, bottom: 5.0, top: 50.0),
                  child: Center(
                    child: AspectRatio(
                      aspectRatio: _controller.value.aspectRatio,
                      child: GestureDetector(
                          onTap: () {
                            isPlaying ? _controller.pause() : _controller.play();
                            setState(() {
                              isPlaying = !isPlaying;
                            });
                          },
                          child: VideoPlayer(_controller)),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 3.0),
                  child: Positioned(
                    bottom: 5,
                    child: FileDescription(
                      description: {
                        'Video duration': '${(_controller.value.duration.inMilliseconds / 1000).toStringAsFixed(2)}s',
                        'Video ratio': Fraction.fromDouble(_controller.value.aspectRatio).reduce().toString(),
                        'Video size': _controller.value.size.toString(),
                      },
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    icon: Icon(Icons.save),
                    onPressed: () {
                      setState(() {
                        if (isPlaying) {
                          _controller.pause();
                          isPlaying = false;
                        }
                        saving = true;
                      });
                      GallerySaver.saveVideo(widget.video.path).then((value) => setState(() {
                            saving = false;
                          }));
                    },
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: IconButton(
                      icon: Icon(CupertinoIcons.xmark),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                ),
                OpacityTransition(
                  visible: saving,
                  child: Align(alignment: Alignment.center, child: CircularProgressIndicator()),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
